﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : SingletonMonoBehaviour<EffectManager> {

    [SerializeField]
    private ParticleSystem _eatenParticles = null;

    public void CallEatenEffect(Vector2 position)
    {
        Instantiate(_eatenParticles, position, Quaternion.identity, this.transform);
    }
}
