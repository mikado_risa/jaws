﻿using UnityEngine;
using System.Collections;

public class ScoreManager : SingletonMonoBehaviour<ScoreManager> {
    
    [SerializeField]
    private int _normalBasicScore = 100;
    [SerializeField]
    private int _heavyBasicScore = 200;
    [SerializeField]
    private int _rareBasicScore = 500;

    //シーン遷移でデータが吹っ飛ぶため臨時で作成
    public static int sNormalResult;
    public static int sHeavyResult;
    public static int sRareResult;
    public static int sTotalScore;

    private int _normalCount;
    public int NormalCount
    {
        get { return _normalCount; }
    }

    private int _heavyCount;
    public int HeavyCount
    {
        get { return _heavyCount; }
    }

    private int _rareCount;
    public int RareCount
    {
        get { return _rareCount; }
    }

    private int _totalScore;
    public int TotalScore
    {
        get { return _totalScore; }
    }

    private bool _isActive;
    public bool IsActive
    {
        get { return _isActive; }
        set { _isActive = value; }
    }

    new void Awake()
    {
        base.Awake();
        _normalCount = 0;
        _heavyCount = 0;
        _rareCount = 0;
        _totalScore = 0;
        _isActive = false;
    }

    public void CountUpNormal()
    {
        if(_isActive)
        {
            _normalCount++;
            UIManager.Instance.ChangeNormalCountDisplay(_normalCount);
        }
    }

    public void CountUpHeavy()
    {
        if(_isActive)
        {
            _heavyCount++;
            UIManager.Instance.ChangeHeavyCountDisplay(_heavyCount);
        }
    }

    public void CountUpRare()
    {
        if(_isActive)
        {
            _rareCount++;
            UIManager.Instance.ChangeRareCountDisplay(_rareCount);
        }
    }

    public void CalcTotalScore()
    {
        _totalScore = 0;
        _totalScore += _normalCount * _normalBasicScore;
        _totalScore += _heavyCount * _heavyBasicScore;
        _totalScore += _rareCount * _rareBasicScore;

        sNormalResult = _normalCount;
        sHeavyResult = _heavyCount;
        sRareResult = _rareCount;
        sTotalScore = _totalScore;
    }
}
