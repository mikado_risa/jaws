﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingManager : SingletonMonoBehaviour<PlayingManager> {

    [SerializeField, Range(1, 300)]
    private int _timeLimit = 180;
    private int _timer;
    private bool _isCoroutiningRoundCall;
    private bool _isCountingDown;

    new void Awake()
    {
        base.Awake();
        _isCountingDown = false;
        _timer = _timeLimit;
    }

    void Start()
    {
        StartCoroutine("StartRoundCall");
    }

    private IEnumerator StartRoundCall()
    {
        if (_isCoroutiningRoundCall) yield break;
        _isCoroutiningRoundCall = true;

        for (int i = 0; i < 30; i++)
        {
            yield return null;
        }

        SoundController.Instance.PlaySEStart();
        UIManager.Instance.ChangeCenterDisplay("READY...");

        for (int i = 0; i < 60; i++)
        {
            yield return null;
        }

        UIManager.Instance.ChangeCenterDisplay("START!!");

        for (int i = 0; i < 60; i++)
        {
            yield return null;
        }

        UIManager.Instance.ChangeCenterDisplay("");

        for (int i = 0; i < 30; i++)
        {
            yield return null;
        }

        SoundController.Instance.PlayBGMStage();
        StartCoroutine("CountDownTimer");
    }

    private IEnumerator CountDownTimer()
    {
        if (_isCountingDown) yield break;
        _isCountingDown = true;

        //ゲーム開始時の処理
        ScoreManager.Instance.IsActive = true;
        UIManager.Instance.StartDisplayingTimer();
        UIManager.Instance.ChangeTimeDisplay(_timeLimit);

        while (_timer > 0)
        {
            for (int i = 0; i < 60; i++)
            {
                yield return null;
            }

            _timer--;
            UIManager.Instance.ChangeTimeDisplay(_timer);
        }

        ScoreManager.Instance.IsActive = false;
        ScoreManager.Instance.CalcTotalScore();
        SoundController.Instance.StopBGMStage();
        SoundController.Instance.PlaySEEnd();
        UIManager.Instance.ChangeCenterDisplay("FINISH!!");

        for (int i = 0; i < 30; i++)
        {
            yield return null;
        }

        FadeManager.Instance.ChangeScene("result", 3.0f);
    }
}
