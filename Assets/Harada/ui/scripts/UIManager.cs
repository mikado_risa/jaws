﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : SingletonMonoBehaviour<UIManager> {

    [SerializeField]
    private Text _centerDisplay = null;
    [SerializeField]
    private Text _timerDisplay = null;
    [SerializeField]
    private Text _normalCountDisplay = null;
    [SerializeField]
    private Text _heavyCountDisplay = null;
    [SerializeField]
    private Text _rareCountDisplay = null;

    new void Awake()
    {
        base.Awake();
        _timerDisplay.enabled = false;
    }

    public void ChangeCenterDisplay(string displayText)
    {
        _centerDisplay.text = displayText;
    }

    public void ChangeNormalCountDisplay(int newCount)
    {
        _normalCountDisplay.text = newCount.ToString();
    }

    public void ChangeHeavyCountDisplay(int newCount)
    {
        _heavyCountDisplay.text = newCount.ToString();
    }

    public void ChangeRareCountDisplay(int newCount)
    {
        _rareCountDisplay.text = newCount.ToString();
    }

    public void ChangeTimeDisplay(int newTime)
    {
        int minute = 0;
        int second = 0;

        minute = newTime / 60;
        second = newTime % 60;

        _timerDisplay.text = minute.ToString() + ":" + second.ToString("00");
    }

    public void StartDisplayingTimer()
    {
        _timerDisplay.enabled = true;
    }
}
