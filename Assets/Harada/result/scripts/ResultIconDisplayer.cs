﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultIconDisplayer : MonoBehaviour {
    
    [SerializeField, Range(1,30)]
    private int _displayIntervalFrame = 15;
    [SerializeField]
    private Image _iconImage = null;

    private bool _hasStartedDisplay;

    void Awake()
    {
        _hasStartedDisplay = false;
    }

    public void StartDisplay(int number)
    {
        StartCoroutine("DisplayingIcon", number);
    }

    private IEnumerator DisplayingIcon(int number)
    {
        if (_hasStartedDisplay) yield break;
        _hasStartedDisplay = true;

        Vector2 displayPosition;
        displayPosition = this.transform.position;
        for (int i = 0; i < number; i++)
        {
            Instantiate(_iconImage, displayPosition, Quaternion.identity, this.transform);
            for (int j = 0; j < _displayIntervalFrame; j++)
            {
                yield return null;
            }
            displayPosition = displayPosition + new Vector2(0.5f, 0.0f);
        }
        ResultManager.Instance.InformDisplayResultFinished();
    }
}
