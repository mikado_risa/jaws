﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManager : SingletonMonoBehaviour<ResultManager>
{

    [SerializeField]
    private int _preWaitFrame = 60;
    [SerializeField]
    private int _intervalWaitFrame = 60;
    [SerializeField]
    private ResultIconDisplayer _normalResultDisplayer = null;
    [SerializeField]
    private ResultIconDisplayer _heavyResultDisplayer = null;
    [SerializeField]
    private ResultIconDisplayer _rareResultDisplayer = null;
    [SerializeField]
    private Text _totalScoreDisplay = null;
    [SerializeField]
    private Text _commentDisplay = null;

    [SerializeField]
    private int _goodScoreBorder = 1000;
    [SerializeField]
    private int _greatScoreBorder = 3000;

    private bool _hasStartedDisplay;
    private int _displayScore;
    private int _finishedDisplayCount;
    private bool _hasStartedDisplayTotalScore;
    private bool _hasStartedDisplayComment;

    [SerializeField]
    List<AudioClip> audioList = new List<AudioClip>();
    [SerializeField]
    AudioSource audioSource = null;

    new void Awake()
    {
        base.Awake();
        _hasStartedDisplay = false;
        _hasStartedDisplayTotalScore = false;
        _hasStartedDisplayComment = false;
        _displayScore = 0;
        _finishedDisplayCount = 0;
    }

    void Start()
    {
        StartCoroutine("StartDisplayingResult");
        StartCoroutine("StartDisplayingTotalScore");
    }

    private IEnumerator StartDisplayingResult()
    {
        if (_hasStartedDisplay) yield break;
        _hasStartedDisplay = true;

        for (int i = 0; i < _preWaitFrame; i++)
            yield return null;

        _normalResultDisplayer.StartDisplay(ScoreManager.sNormalResult);

        for (int i = 0; i < _intervalWaitFrame; i++)
            yield return null;

        _heavyResultDisplayer.StartDisplay(ScoreManager.sHeavyResult);

        for (int i = 0; i < _intervalWaitFrame; i++)
            yield return null;

        _rareResultDisplayer.StartDisplay(ScoreManager.sRareResult);
    }

    public void InformDisplayResultFinished()
    {
        _finishedDisplayCount++;
    }

    private IEnumerator StartDisplayingTotalScore()
    {
        if (_hasStartedDisplayTotalScore) yield break;
        _hasStartedDisplayTotalScore = true;

        while (true)
        {
            for (int i = 0; i < 6; i++)
            {
                yield return null;
            }

            if (_finishedDisplayCount == 3)
            {
                //ジャラジャラジャラジャラ・・・
                for (int i = 0; i < 30; i++)
                {
                    DisplayTotalScoreRandomly();
                    for (int j = 0; j < 2; j++)
                    {
                        yield return null;
                    }
                }

                _totalScoreDisplay.text = "";
                for (int i = 0; i < 30; i++)
                {
                    yield return null;
                }

                //総合スコアを計算し表示
                //ScoreManager.Instance.CalcTotalScore();
                //_totalScoreDisplay.text = ScoreManager.Instance.TotalScore.ToString();
                _totalScoreDisplay.text = ScoreManager.sTotalScore.ToString();
                break;
            }
            else
            {
                DisplayTotalScoreRandomly();
            }
        }

        StartCoroutine("StartDisplayingComment");
    }

    private void DisplayTotalScoreRandomly()
    {
        _displayScore = Random.Range(1000, 9999);
        _totalScoreDisplay.text = _displayScore.ToString();
    }

    private IEnumerator StartDisplayingComment()
    {
        if (_hasStartedDisplayComment) yield break;
        _hasStartedDisplayComment = true;

        for (int i = 0; i < 30; i++) yield return null;

        string displayComment;

        if (ScoreManager.sTotalScore > _greatScoreBorder)
        {
            audioSource.clip = audioList[0];
            displayComment = "ウルトラジョーズに\n　　　　　食べました！";
        }
        else if (ScoreManager.sTotalScore > _goodScoreBorder)
        {
            audioSource.clip = audioList[1];

            displayComment = "ジョーズに食べました！";
        }
        else
        {
            audioSource.clip = audioList[2];
            displayComment = "残さず食べましょう。";
        }
        audioSource.Play();
        _commentDisplay.text = displayComment;
    }

    public void ClickEndButtone()
    {
        audioSource.clip = audioList[3];
        audioSource.Play();
    }
}
