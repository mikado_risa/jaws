﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour {
	
	private GameObject playerObject;
	//自身が落とされているかどうか
	private bool isShooted;
	public bool IsShooted { get { return isShooted; } set { isShooted = value; } }
	//自身(Boss)がもがいているかどうか
	private bool isMogaki;
	public bool IsMogaki { get { return isMogaki; } set { isMogaki = value;} }
	//もがく直前の座標変数
	private Vector2 mogakiInitPos;
	public void setMogakiInitPos(Vector2 pos){
		mogakiInitPos = pos;
	}

	//発射後ある程度時間が経ったら削除する為の制限時間
	[SerializeField]
	private int bossLifeTimeMax;
	private int bossLifeTime;

	//お金エフェクト格納変数
	[SerializeField]
	private GameObject MoneyParticle;

	void Start () {
		bossLifeTime = 0;
		IsShooted = false;
		IsMogaki = false;
		playerObject = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update () {
		if (!IsShooted) {
			if (IsMogaki) {
				mogakiInitPos = new Vector2 (playerObject.transform.position.x , playerObject.transform.position.y - 1);
				gameObject.transform.position = new Vector2 (mogakiInitPos.x + Random.Range (-0.2f, 0.2f), mogakiInitPos.y + Random.Range (-0.2f, 0.2f));
			} else {
				gameObject.transform.position = new Vector2 (playerObject.transform.position.x, playerObject.transform.position.y - 1);
			}
		} else {
			bossLifeTime++;
			if (bossLifeTime > bossLifeTimeMax) {
				Destroy (this.gameObject);
			}
		}
	}

	//衝突判定
	void OnTriggerEnter2D(Collider2D c){
		if (c.gameObject.CompareTag ("BossDeadArea") || c.gameObject.CompareTag("MoneyBird")) {
			if (c.CompareTag ("MoneyBird")) {
				if (this.gameObject.GetComponent<EffectManager> () != null) {
					this.gameObject.GetComponent<EffectManager> ().CallEatenEffect (this.gameObject.transform.position);
				}
			}
			//デットゾーンで入った場合このオブジェクトを削除する(今回は上司)
			Destroy (this.gameObject);
		}
	}
}
