﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	//プレイヤーのデフォルト加速値、0.1~0.9が丁度良い
	[SerializeField]
	private float playerSpeed = 0.1f;
	//ゲーム中のプレイヤースピード
	private float nowPlayerSpeed;
	//上司のゲームオブジェクト
	[SerializeField]
	private GameObject NormalBossPrefab;	//ノーマル上司
	[SerializeField]
	private GameObject HeavyBossPrefab;		//ヘビー上司
	[SerializeField]
	private GameObject RareBossPrefab;		//レア上司
	//各上司の持ち上げ持続時間
	[SerializeField]
	private float NormalBossHaveTime;		//ノーマル上司
	[SerializeField]
	private float HeavyBossHaveTime;		//ヘビー上司
	[SerializeField]
	private float RareBossHaveTime;			//レア上司
	//持ち続けている時間格納変数
	private float haveTime;
	//各上司がもがいている時のプレイヤースピード減速幅
	[SerializeField]
	private float NormalBossDownSpeed;		//ノーマル上司
	[SerializeField]
	private float HeavyBossDownSpeed;		//ヘビー上司
	[SerializeField]
	private float RareBossDownSpeed;		//レア上司

	//落下音
	private AudioSource fallAudio;
	//悲鳴音
	private AudioSource screamAudio;

	//上司がもがいているかどうか
	private bool isBossMogaki;
	//もがいている時の吹き出しオブジェクト
	[SerializeField]
	private GameObject popPrefab;

	//持ち上げている上司のゲームオブジェクト格納庫
	private GameObject BossObject;
	//お金エフェクト格納変数
	[SerializeField]
	private GameObject MoneyParticlePrefab;

	//レア上司の横移動変動値(高ければ高いほど横に飛びやすくなる)
	[SerializeField]
	private float rareBossSideMove;

	[SerializeField]
	private float HeavyWeight = -30.0f;

	//上司を持ち上げているか否か
	private bool isBossHaved = false;
	public bool IsBossHaved { get { return isBossHaved; } } 

	//プレイヤーの速度限界値
	private float maxPlayerSpeed = 0.5f;

	void Start(){
		nowPlayerSpeed = playerSpeed;
		isBossMogaki = false;
		AudioSource[] audios = gameObject.GetComponents<AudioSource> ();
		fallAudio = audios [0];
		screamAudio = audios [1];
	}
		
	void Update () {
		Move ();
		//スペースキーが押されているかつ上司を持ち上げている(true)場合->上司を落とす(false)
		if (Input.GetKeyUp (KeyCode.Return) && isBossHaved) {
			BossShoot ();
		}
		HaveManage ();
	}

	void Move(){
		Vector2 playerPosition = gameObject.transform.position;

		//移動キーを押し続けた時の挙動管理
		SpeedManage ();

		if (Input.GetKey ("left")) {
			playerPosition.x -= nowPlayerSpeed;
		}
		if (Input.GetKey ("right")) {
			playerPosition.x += nowPlayerSpeed;
		}
		//カメラの左下と右上の座標を取得
		Vector2 minClamp = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		Vector2 maxClamp = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
		//移動反映
		gameObject.transform.position = new Vector2 (Mathf.Clamp (playerPosition.x, minClamp.x, maxClamp.x), Mathf.Clamp (playerPosition.y, minClamp.y, maxClamp.y));
	}

	void BossShoot(){
		fallAudio.Play ();
		popPrefab.SetActive (false);
		isBossHaved = false;
		haveTime = 0;
		if (BossObject != null) {
			BossObject.GetComponent<Rigidbody2D> ().gravityScale = 1.0f;
			if (BossObject.CompareTag ("HeavyBoss")) {
				Vector2 vec = new Vector2 (0.0f, HeavyWeight * -1);
				BossObject.GetComponent<Rigidbody2D> ().AddForce (vec, ForceMode2D.Impulse);
			} else if (BossObject.CompareTag ("RareBoss")) {
				Vector2 vec = new Vector2 (Random.Range (-1 * rareBossSideMove, rareBossSideMove), 0.0f);
				BossObject.GetComponent<Rigidbody2D> ().AddForce (vec, ForceMode2D.Impulse);
				//お金エフェクトの追加挿入
				Instantiate (MoneyParticlePrefab, BossObject.transform);
			}
			BossObject.GetComponent<BossController> ().IsShooted = true;
		}
	}

	void SpeedManage(){
		if ((Input.GetKey ("left") || Input.GetKey ("right")) && nowPlayerSpeed < 0.5f)
			nowPlayerSpeed += 0.01f;
		else if(nowPlayerSpeed > 0.05f)
			nowPlayerSpeed -= 0.1f;
	}

	void OnTriggerStay2D(Collider2D c){
		//右シフトキーが押されているかつ上司を持っていない(false)かつ背面にヘリが存在する場合->上司をセット(true)する
		if (Input.GetKeyDown (KeyCode.Return) && !isBossHaved && c.gameObject.CompareTag ("Heli")) {
			isBossHaved = true;
			popPrefab.SetActive (true);
			int spownBossRnd = Random.Range (0, 100);
			if (spownBossRnd >= 0 && spownBossRnd <= 60) {
				BossObject = Instantiate (NormalBossPrefab, new Vector2 (transform.position.x, transform.position.y - 0.5f), NormalBossPrefab.transform.rotation);
			} else if (spownBossRnd >= 61 && spownBossRnd <= 80) {
				BossObject = Instantiate (HeavyBossPrefab, new Vector2 (transform.position.x, transform.position.y - 0.5f), HeavyBossPrefab.transform.rotation);
			} else if (spownBossRnd >= 81 && spownBossRnd <= 100) {
				BossObject = Instantiate (RareBossPrefab, new Vector2 (transform.position.x, transform.position.y - 0.5f), RareBossPrefab.transform.rotation);
			}
		}
	}

	void HaveManage(){
		//上司を持ち続けていた場合の持続管理
		//一定時間持ち続けていた場合こちらで自動的に離す(一定時間は上司ごとに異なる)
		if (isBossHaved && BossObject != null) {
			haveTime++;
			//一定時間を半分超した場合->上司がもがく
			if (BossObject.CompareTag ("NormalBoss") && haveTime > NormalBossHaveTime / 4){		//ノーマル上司
				screamAudio.Play();
				nowPlayerSpeed = playerSpeed;
				BossObject.GetComponent<BossController> ().IsMogaki = true;
				BossObject.GetComponent<BossController> ().setMogakiInitPos(BossObject.transform.position);
				nowPlayerSpeed -= Random.Range (NormalBossDownSpeed * -1, NormalBossDownSpeed);
			}
			if (BossObject.CompareTag ("HeavyBoss") && haveTime > HeavyBossHaveTime / 4){		//ヘビー上司
				screamAudio.Play();
				nowPlayerSpeed = playerSpeed;
				BossObject.GetComponent<BossController> ().IsMogaki = true;
				BossObject.GetComponent<BossController> ().setMogakiInitPos (BossObject.transform.position);
				nowPlayerSpeed -= Random.Range (HeavyBossDownSpeed * -1, HeavyBossDownSpeed);
			}
			if (BossObject.CompareTag ("RareBoss") && haveTime > RareBossHaveTime / 4){			//レア上司
				screamAudio.Play();
				nowPlayerSpeed = playerSpeed;
				BossObject.GetComponent<BossController> ().IsMogaki = true;
				BossObject.GetComponent<BossController> ().setMogakiInitPos (BossObject.transform.position);
				nowPlayerSpeed -= Random.Range (RareBossDownSpeed * -1, RareBossDownSpeed);
			}
			//一定時間を超えていた場合->上司を離す
			if ((BossObject.CompareTag ("NormalBoss") && haveTime > NormalBossHaveTime) ||
				(BossObject.CompareTag("HeavyBoss") && haveTime > HeavyBossHaveTime) ||
				(BossObject.CompareTag("RareBoss") && haveTime > RareBossHaveTime)) {
				BossShoot ();
			}
		}
	}
}