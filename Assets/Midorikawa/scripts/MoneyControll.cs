﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyControll : MonoBehaviour
{
    [SerializeField]
    Vector3 initialPosition;   // 初期座標

    [SerializeField]
    float MoneySpeedX;          // x軸方向のスピード

    [SerializeField]
    float MoneySpeedY;          // x軸方向のスピード

    [SerializeField]
    int RandMax;          // お金の発生確率

    [SerializeField]
    float Radius;      // ヘリの動く半径

    private bool Move = false;
    private bool Right = false;

    private int RandNum;

    // Use this for initialization
    void Start()
    {
       
        transform.position = new Vector3(initialPosition.x, initialPosition.y, initialPosition.z);

    }

    // Update is called once per frame
    void Update()
    {

        if (Move == true)
        {
            // 移動
            transform.Translate(MoneySpeedX, Mathf.Sin(Time.time / Radius) * MoneySpeedY, 0);

            if (Right == false)
            {
                // 画面外にでたら
                if (transform.position.x < -15)
                {
                    // 
                    Move = false;
                    // そのオブジェクトを消す.
                    this.gameObject.SetActive(false);
                }
            }
            else if(Right == true)
            {
                // 画面外にでたら
                if (transform.position.x > 15)
                {
                    // 
                    Move = false;
                    // そのオブジェクトを消す.
                    Destroy(this.gameObject);
                }
            }
        }
        else if (Move == false)
        {
            // 確率で札を発生
            RandNum = Random.Range(0, RandMax);
            if (RandNum == 1)
            {
                // さらに乱数を発生させて
                RandNum = Random.Range(0, 1);

                // ２分の１の確率で
                if (RandNum == 0)
                {
                    // 反対向きに
                    MoneySpeedX *= -1.0f;
                    initialPosition.x *= -1.0f;

                    // 右向いているなら
                    if (Right == true)
                    {
                        // 左
                        Right = false;
                    }

                    // 左向いているなら
                    else if (Right == false)
                    {
                        // 右
                        Right = true;
                    }
                }

                Move = true;

                // 表示させて
                GameObject Money = Instantiate(this.gameObject) as GameObject;

                // 座標セット
                Money.transform.position = new Vector3(initialPosition.x, initialPosition.y, initialPosition.z);

            }
        }
    }
}