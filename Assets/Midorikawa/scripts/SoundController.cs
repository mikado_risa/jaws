﻿using UnityEngine;
using System;
using System.Collections;


// 音管理クラス
public class SoundController : MonoBehaviour
{

    protected static SoundController instance;

    public static SoundController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (SoundController)FindObjectOfType(typeof(SoundController));

                if (instance == null)
                {
                    Debug.LogError("SoundController Instance Error");
                }
            }

            return instance;
        }
    }

    // 音量
    public SoundVolume volume = new SoundVolume();

    // === AudioSource ===
    // BGM
    private AudioSource BGMsource;

    private AudioSource HeliSource;
    // SE
    private AudioSource[] SEsources = new AudioSource[16];

    // === AudioClip ===
    // BGM
    public AudioClip BGMStage;
    // SE
    public AudioClip SEJump;
    // SE
    public AudioClip SEFall;
    // SE
    public AudioClip SEEat;
    // SE
    public AudioClip SEHeli;
    // SE
    public AudioClip SEStart;
    // SE
    public AudioClip SEEnd;

    void Awake()
    {
        GameObject[] obj = GameObject.FindGameObjectsWithTag("SoundController");
        if (obj.Length > 1)
        {
            // 既に存在しているなら削除
            Destroy(gameObject);
        }
        else
        {
            // 音管理はシーン遷移では破棄させない
            DontDestroyOnLoad(gameObject);
        }

        // 全てのAudioSourceコンポーネントを追加する

        // BGM AudioSource
        BGMsource = gameObject.AddComponent<AudioSource>();
        // BGMはループを有効にする
        BGMsource.loop = true;

        // ヘリ AudioSource
        HeliSource = gameObject.AddComponent<AudioSource>();
        // ヘリはループを有効にする
        HeliSource.loop = true;

        // SE AudioSource
        for (int i = 0; i < SEsources.Length; i++)
        {
            SEsources[i] = gameObject.AddComponent<AudioSource>();
        }

        // ミュート設定
        BGMsource.mute = volume.Mute;
        foreach (AudioSource source in SEsources)
        {
            source.mute = volume.Mute;
        }

        // ボリューム設定
        BGMsource.volume = volume.BGM;
        foreach (AudioSource source in SEsources)
        {
            source.volume = volume.SE;
        }
    }

    void Update()
    {
        // ミュート設定
        BGMsource.mute = volume.Mute;
        foreach (AudioSource source in SEsources)
        {
            source.mute = volume.Mute;
        }

        // ボリューム設定
        BGMsource.volume = volume.BGM;
        foreach (AudioSource source in SEsources)
        {
            //source.volume = volume.SE;
        }
    }

    // ***** BGM再生 *****
    // BGM再生
    public void PlayBGMStage()
    {
        // 同じBGMの場合は何もしない
        if (BGMsource.clip == BGMStage)
        {
            return;
        }
        BGMsource.Stop();
        BGMsource.clip = BGMStage;
        BGMsource.Play();
    }

    // BGM停止
    public void StopBGMStage()
    {
        BGMsource.Stop();
        BGMsource.clip = null;
    }


    // ***** SE再生 *****
    // SE再生
    public void PlaySEJump()
    {
        // 再生中で無いAudioSouceで鳴らす
        foreach (AudioSource source in SEsources)
        {
            if (false == source.isPlaying)
            {
                source.clip = SEJump;
                source.Play();
                return;
            }
        }
    }

    // SE停止
    public void StopSEJump()
    {
        // 全てのSE用のAudioSouceを停止する
        foreach (AudioSource source in SEsources)
        {
            source.Stop();
            source.clip = null;
        }
    }
    // ***** SE再生 *****
    // SE再生
    public void PlaySEFall()
    {
        // 再生中で無いAudioSouceで鳴らす
        foreach (AudioSource source in SEsources)
        {
            if (false == source.isPlaying)
            {
                source.clip = SEFall;
                source.Play();
                return;
            }
        }
    }

    // SE停止
    public void StopSEFall()
    {
        // 全てのSE用のAudioSouceを停止する
        foreach (AudioSource source in SEsources)
        {
            source.Stop();
            source.clip = null;
        }
    }

    // ***** SE再生 *****
    // SE再生
    public void PlaySEEat()
    {
        // 再生中で無いAudioSouceで鳴らす
        foreach (AudioSource source in SEsources)
        {
            if (false == source.isPlaying)
            {
                source.clip = SEEat;
                source.Play();
                return;
            }
        }
    }

    // SE停止
    public void StopSEEat()
    {
        // 全てのSE用のAudioSouceを停止する
        foreach (AudioSource source in SEsources)
        {
            source.Stop();
            source.clip = null;
        }


    }

    // ***** SE再生 *****
    // SE再生
    public void PlaySEHeli()
    {
        // 再生中で無いAudioSouceで鳴らす
        foreach (AudioSource source in SEsources)
        {
            if (false == source.isPlaying)
            {
                source.clip = SEHeli;
                source.volume = 0.125f;
                source.Play();
                return;
            }
        }
    }

    // SE停止
    public void StopSEHeli()
    {
        //// 全てのSE用のAudioSouceを停止する
        //foreach (AudioSource source in SEsources)
        //{
        //    source.Stop();
        //    source.clip = null;
        //}
        // 同じBGMの場合は何もしない
        if (HeliSource.clip == SEHeli)
        {
            return;
        }
        HeliSource.Stop();
        HeliSource.clip =SEHeli;
        HeliSource.Play();
    }

    // ***** SE再生 *****
    // SE再生
    public void PlaySEStart()
    {
        // 再生中で無いAudioSouceで鳴らす
        foreach (AudioSource source in SEsources)
        {
            if (false == source.isPlaying)
            {
                source.clip = SEStart;
                source.Play();
                return;
            }
        }
    }

    // SE停止
    public void StopSEStart()
    {
        // 全てのSE用のAudioSouceを停止する
        foreach (AudioSource source in SEsources)
        {
            source.Stop();
            source.clip = null;
        }
    }

    // ***** SE再生 *****
    // SE再生
    public void PlaySEEnd()
    {
        // 再生中で無いAudioSouceで鳴らす
        foreach (AudioSource source in SEsources)
        {
            if (false == source.isPlaying)
            {
                source.clip = SEEnd;
                source.Play();
                return;
            }
        }
    }

    // SE停止
    public void StopSEEnd()
    {
        // 全てのSE用のAudioSouceを停止する
        foreach (AudioSource source in SEsources)
        {
            source.Stop();
            source.clip = null;
        }
    }

}

// 音量クラス
[Serializable]
public class SoundVolume
{
    public float BGM = 1.0f;
    public float Voice = 1.0f;
    public float SE = 1.0f;
    public bool Mute = false;

    public void Init()
    {
        BGM = 1.0f;
        Voice = 1.0f;
        SE = 1.0f;
        Mute = false;
    }
}