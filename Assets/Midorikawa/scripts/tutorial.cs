﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorial : MonoBehaviour {

    private bool flg = false;

	// Use this for initialization
	void Start () {
        flg = false;
    }

    // Update is called once per frame
    void Update() {

        // 一度だけ
        if (flg == false)
        {
            if (Input.GetKeyUp(KeyCode.Q))
            {
                flg = true;

                // シーン遷移
                FadeManager.Instance.ChangeScene(("PlayingCoop"), 2.0f);
            }
        }
    }
}
