﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeliController : MonoBehaviour
{

    [SerializeField]
    Vector3 initialPosition;   // 初期座標

    [SerializeField]
    float MoveSpeed = 2.0f;    // ヘリのスピード

    [SerializeField]
    float Radius = 10.0f;      // ヘリの動く半径

    [SerializeField]
    float Roty;                // 回転角度

    [SerializeField]
    float RotSpeed = 30.0f;    // ヘリ回転ピード

    void Start()
    {
        SoundController.Instance.PlaySEHeli();
    }

    // Update is called once per frame
    void Update()
    {

        //if (Stop == false)
        //{
        // 移動
        transform.position = new Vector3(Mathf.Sin(Time.time / MoveSpeed) * Radius + initialPosition.x, initialPosition.y, initialPosition.z);

        // 回転角度を更新
        Roty += Time.deltaTime * RotSpeed;

        // 回転処理
        transform.rotation = Quaternion.Euler(0, Roty, 0);
        //}

        SoundController.Instance.PlaySEHeli();
    }
}