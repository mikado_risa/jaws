﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeManager : MonoBehaviour {

	// ----------------------------------------
	// singleton
	private static FadeManager mInstance;

	private FadeManager() {
		//Debug.Log ("Create FadeManager GameObject.");
	}

	public static FadeManager Instance {
		get {
			if (mInstance == null) {
				GameObject obj = new GameObject ("FadeManager");
				mInstance = obj.AddComponent<FadeManager> ();
			}
			return mInstance;
		}
	}
	// ----------------------------------------

	// ----------------------------------------
	// fade用のテクスチャなど
	private Texture2D fadeTexture;
	private float fadeAlpha = 0;
	private bool isFading = false;
	// ----------------------------------------

	// ----------------------------------------
	// 初期化処理
	void Awake () {
		Debug.Log ("FadeManager is start");
		// fede用テクスチャ作成
		fadeTexture = new Texture2D (32, 32, TextureFormat.RGB24, false);
		fadeTexture.SetPixel (0, 0, Color.white);
		fadeTexture.Apply ();

		// signletonオブジェクトを削除したくない
		DontDestroyOnLoad(this.gameObject);
	}
	// ----------------------------------------
		
	// ----------------------------------------
	// テクスチャ描画
	void OnGUI(){
		if (!isFading) {
			return;
		}
		GUI.color = new Color (0, 0, 0, fadeAlpha);
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadeTexture);
	}
	// ----------------------------------------

	// ----------------------------------------
	// フェード中かどうか取得
	public bool isFade() {
		return isFading;
	}

	// ----------------------------------------
	// シーン遷移
	public void ChangeScene(string scene, float interval) {
		StartCoroutine (ChangeSceneCoroutine (scene, interval));
	}

	// フェードイン->フェードアウトの間にシーン遷移のコルーチン
	private IEnumerator ChangeSceneCoroutine(string scene, float interval) {
		isFading = true;
		float time = 0.0f;
		// フェードイン
		while (time <= interval) {
			fadeAlpha = Mathf.Lerp (0.0f, 1.0f, time / interval);
			time += Time.deltaTime;
			yield return 0;
		}

		// シーンのロード
		SceneManager.LoadScene (scene);
		time = 0;

		// フェードアウト
		while (time <= interval) {
			fadeAlpha = Mathf.Lerp (1.0f, 0.0f, time / interval);
			time += Time.deltaTime;
			yield return 0;
		}

		isFading = false;
	}
	// ----------------------------------------

    // ----------------------------------------
    // ゲーム終了 (原田追加)
    public void GameEnd(float interval)
    {
        StartCoroutine(GameEndCoroutine(interval));
    }

    // フェードイン->フェードアウトの間にシーン遷移のコルーチン
    private IEnumerator GameEndCoroutine(float interval)
    {
        isFading = true;
        float time = 0.0f;
        // フェードイン
        while (time <= interval)
        {
            fadeAlpha = Mathf.Lerp(0.0f, 1.0f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        // ゲーム終了
        time = 0;
        isFading = false;
        Application.Quit();
    }
    // ----------------------------------------

	// フェードインだけコルーチン
	private IEnumerator FadeInCoroutine(float interval) {
		isFading = true;
		float time = 0.0f;
		while (time <= interval) {
			fadeAlpha = Mathf.Lerp (0.0f, 1.0f, time / interval);
			time += Time.deltaTime;
			yield return 0;
		}
	}

	// フェードアウトだけコルーチン
	public IEnumerator FadeOutCoroutine(float interval) {
		isFading = true;
		float time = 0.0f;
		while (time <= interval) {
			fadeAlpha = Mathf.Lerp (1.0f, 0.0f, time / interval);
			time += Time.deltaTime;
			yield return 0;
		}
	}
}
