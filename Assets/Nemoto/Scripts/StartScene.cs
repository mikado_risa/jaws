﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScene : MonoBehaviour {

	private GameObject boss;
	private BossController bossControllerComp;
	private bool isPressedSpace = false;
	private float timer = 0.0f;

	public float waitTime = 1.0f;

	// Use this for initialization
	void Start () {
		isPressedSpace = false;
		timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		// NormalBossを落とす
		if (!isPressedSpace && Input.GetKeyDown (KeyCode.Space)) {
			isPressedSpace = true;
			boss = GameObject.FindGameObjectWithTag ("NormalBoss");
			// 念のためbossインスタンスがあるか確認
			if (boss != null) {
				bossControllerComp = boss.GetComponent<BossController> ();
				boss.GetComponent<Rigidbody2D> ().gravityScale = 1.0f;
				bossControllerComp.IsShooted = true;
			}
		}
		// spaceボタン押されてから一定時間後シーン遷移開始
//		if (isPressedSpace) {
//			timer += Time.deltaTime;
//			if (timer >= waitTime) {
//				isPressedSpace = false;
//				FadeManager.Instance.ChangeScene ("PlayingCoop", 2.0f);
//			}
//		}
	}
}
