﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JowsSimple : MonoBehaviour {
	private bool nowJumping = false;
	public float nowJumpVelocity = 0.0f;
	[SerializeField]
	private float fallVelocity = 1f;
	[SerializeField]
	private float InitialVelocity = 4f;

	public float underPositionY = -4.67f;

    [SerializeField]
    List<AudioSource> soucerList = new List<AudioSource>();

    private enum Sound
    {
        eat,
        scream,
        jump,
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// jumpボタン押されたか
		if (nowJumping == false && Input.GetKeyDown (KeyCode.Space)) {
			nowJumping = true;
			nowJumpVelocity = InitialVelocity;
            soucerList[(int)Sound.jump].Play();

        }

		// jump処理
		if (nowJumping == true) {
			UpdateJump ();
		}

		// 下に落ち切っていたとき
		if (gameObject.transform.position.y < underPositionY) {
			// 位置の補正
			gameObject.transform.position = new Vector2 (gameObject.transform.position.x, underPositionY);
			// 下まで落ち切ったのでフラグを戻す
			nowJumping = false;
		}
	}

	// jump時の処理
	private void UpdateJump()
	{
		nowJumpVelocity -= fallVelocity;
		// nowJumpVelocityで動かす
		gameObject.transform.position = new Vector2 (gameObject.transform.position.x, gameObject.transform.position.y + nowJumpVelocity);
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (col.tag == "NormalBoss") {
			// effect出す
			EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);
			// 当たったやつ壊す
			Destroy(col.gameObject);
            soucerList[(int)Sound.eat].Play();
            soucerList[(int)Sound.scream].Play();
        }
    }
}
