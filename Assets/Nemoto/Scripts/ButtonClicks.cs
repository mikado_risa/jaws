﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonClicks : MonoBehaviour {

	public float changeSceneInterval = 2.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Gameシーンへ遷移
	public void OnClickToGameButton() {
		Debug.Log ("StartButton is clicked.");
		//SceneManager.LoadScene ("Game");
		if (!FadeManager.Instance.isFade ()) {
			FadeManager.Instance.ChangeScene ("PlayingCoop", 2.0f);
		}
	}

	// Resultシーンへ遷移
	public void OnClickToResultButton() {
		Debug.Log ("ToResultButton is clicked.");
		//SceneManager.LoadScene ("Result");
		if (!FadeManager.Instance.isFade ()) {
			FadeManager.Instance.ChangeScene ("Result", 2.0f);
		}
	}

	// Startシーンへ遷移
	public void OnClickToStartButton() {
		Debug.Log ("ToStartButton is clicked.");
		//SceneManager.LoadScene ("Start");
		if (!FadeManager.Instance.isFade ()) {
			FadeManager.Instance.ChangeScene ("Start", 2.0f);
		}
	}

    // Tutorialシーンへ遷移
    public void OnClickToTutorialButton()
    {
        Debug.Log("ToTutorialButton is clicked.");
        //SceneManager.LoadScene ("Start");
		if (!FadeManager.Instance.isFade ()) {
			FadeManager.Instance.ChangeScene ("Tutorial", 2.0f);
		}
    }

    // ゲーム終了
    public void OnClickToGameEndButton()
    {
        Debug.Log("ToGameEndButton is clicked.");
        //SceneManager.LoadScene ("Start");
        FadeManager.Instance.GameEnd(3.0f);
    }
}
