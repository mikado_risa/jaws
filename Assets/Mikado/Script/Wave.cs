﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{

    [SerializeField]
    float addSpeed = 0.01f;
    [SerializeField]
    float TopY = 0.01f;
    [SerializeField]
    float DownY = 0.01f;

    [SerializeField]
    float FrontTopY = 0.01f;


    [SerializeField]
    GameObject waveFront = null;

    [SerializeField]
    GameObject waveBack = null;

    bool frontUP = false;
    bool BackUP = false;


    // Use this for initialization
    void Start()
    {

        SetUpFlag(frontUP);
        SetUpFlag(BackUP);

    }

    private void SetUpFlag(bool setUpFlag)
    {
        int rand = Random.Range(0, 2);

        if (rand == 0)
            setUpFlag = true;
        else
            setUpFlag = false;
    }

    // Update is called once per frame
    void Update()
    {

        UpdateWave(waveBack, ref BackUP);
        if (frontUP)
        {
            waveFront.transform.position += new Vector3(0, addSpeed, 0);
            if (waveFront.transform.position.y > FrontTopY)
                frontUP = false;

        }
        else
        {
            waveFront.transform.position += new Vector3(0, -addSpeed, 0);
            if (waveFront.transform.position.y < DownY)
                frontUP = true;
        }

    }

    private void UpdateWave(GameObject moveObject,ref bool nowUP)
    {
        if(nowUP)
        {
            moveObject.transform.position += new Vector3(0, addSpeed, 0);
            if(moveObject.transform.position.y>TopY)
                nowUP = false;

        }
        else
        {
            moveObject.transform.position += new Vector3(0, -addSpeed, 0);
            if (moveObject.transform.position.y <DownY)
                nowUP = true;
        }
    }
}
