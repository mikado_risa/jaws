﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;	// y.nemoto 追加

public class Jaws : MonoBehaviour
{

    float accelaletion = 0;
    [SerializeField]
    float addAccelaletion = 0.1f;
    [SerializeField]
    float Deceleration = 2.5f;

    [SerializeField]
    float LowestSpeed = 0.03f;

    float oldAccelaletion = 0;

    Rigidbody2D jawsRigidbody2D = null;

    bool addX = false;





    bool nowJumping = false;

    float underPositionY = -4.67f;
    [SerializeField]
    float InitialVelocity = 4f;

    [SerializeField]
    float fallVelocity = 1f;

    [SerializeField]
    float wallReflection = 1.5f;

    float nowJumpVelocity = 0.0f;

    bool pushButtone = false;

	private string sceneName = "";	// y.nemoto 追加

    void Start()
    {
        oldAccelaletion = accelaletion = LowestSpeed;
        jawsRigidbody2D = this.GetComponent<Rigidbody2D>();
        underPositionY = jawsRigidbody2D.position.y;
		sceneName = SceneManager.GetActiveScene ().name;	// y.nemoto 追加
    }


    // Update is called once per frame
    void Update()
    {
        pushButtone = false;
        if (nowJumping == true)
            UpdateJump();

        //  float force = 105f;

        

        if (Input.GetKey(KeyCode.Space) && nowJumping == false)
        {
            SoundController.Instance.PlaySEJump();
            nowJumping = true;
            nowJumpVelocity = InitialVelocity;
        }
        else if (Input.GetKey(KeyCode.A))
        {

            accelaletion -= addAccelaletion;
            addX = false;
            pushButtone = true;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            accelaletion += addAccelaletion;
            addX = true;
            pushButtone = true;
        }


        accelaletion /= Deceleration;


        if (accelaletion <=0 && -LowestSpeed < accelaletion && pushButtone == false)
            accelaletion = -LowestSpeed;
        else if (accelaletion >=0 && LowestSpeed > accelaletion && pushButtone == false)
            accelaletion = LowestSpeed;

        oldAccelaletion = accelaletion;

        jawsRigidbody2D.position = new Vector2(jawsRigidbody2D.position.x + accelaletion, jawsRigidbody2D.position.y + nowJumpVelocity);
    }

    private void UpdateJump()
    {
        nowJumpVelocity -= fallVelocity;
        if (jawsRigidbody2D.position.y <= underPositionY)
        {
            nowJumping = false;
            nowJumpVelocity = 0f;
            jawsRigidbody2D.position = new Vector2(jawsRigidbody2D.position.x, underPositionY);
        }
    }

    [SerializeField]
    float addSpeed = 0.04f;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Wall")
        {
            accelaletion = -accelaletion / wallReflection;
            oldAccelaletion = -oldAccelaletion / wallReflection;

            addX = !addX;

            //加速度が一定以下なら強制的に代入する
            if (Mathf.Abs(accelaletion) < addSpeed)
            {
                
                if (accelaletion < 0)
                    oldAccelaletion = accelaletion = -addSpeed;
                else
                    oldAccelaletion = accelaletion = addSpeed;

            }


        }
        else if (col.tag == "NormalBoss")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);

			// Tutorrialシーンでは呼ばれないようにした y.nemoto
			if (sceneName != "Tutorial") {
				ScoreManager.Instance.CountUpNormal ();
			}
            Destroy(col.gameObject);
        }
        else if (col.tag == "HeavyBoss")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);

			// Tutorrialシーンでは呼ばれないようにした y.nemoto
			if (sceneName != "Tutorial") {
				ScoreManager.Instance.CountUpHeavy();
			}
            Destroy(col.gameObject);
        }
        else if (col.tag == "RareBoss")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);

			// Tutorrialシーンでは呼ばれないようにした y.nemoto
			if (sceneName != "Tutorial") {
				ScoreManager.Instance.CountUpRare ();
			}
            Destroy(col.gameObject);
        }
        else if (col.tag == "MoneyBird")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);
            Destroy(col.gameObject);
        }

    }



}
