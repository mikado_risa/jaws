﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Right,
    Left,
}

public class GiantJaws : MonoBehaviour
{

    [SerializeField]
    float moveSpeed = 0.5f;


    public Direction moveDirection = Direction.Right;
    Rigidbody2D jawsRigidbody2D = null;
    int hitCount = 0;

    public void SetDirection(Direction d)
    {
        moveDirection = d;
    }

    //汚いが機能重視
    float endTimer = 0.0f;

    // Use this for initialization
    void Start()
    {
        jawsRigidbody2D = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (moveDirection == Direction.Right)
            jawsRigidbody2D.AddForce(new Vector2(moveSpeed, 0));
        else
            jawsRigidbody2D.AddForce(new Vector2(-moveSpeed, 0));

        if(hitCount>=2)
        {
            endTimer += Time.deltaTime;
            if(endTimer>=2)//端の壁に当たって消えるまで
            {
                Destroy(this.gameObject);
            }
        }

    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Wall")
        {
            hitCount++;
            //if(hitCount>=2)
            //Destroy(this.gameObject);
        }
        else if (col.tag == "NormalBoss")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);
            Destroy(col.gameObject);
        }
        else if (col.tag == "HeavyBoss")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);
            Destroy(col.gameObject);
        }
        else if (col.tag == "RareBoss")
        {
            SoundController.Instance.PlaySEEat();
            EffectManager.Instance.CallEatenEffect(col.gameObject.transform.position);
            Destroy(col.gameObject);
        }
    }
}
