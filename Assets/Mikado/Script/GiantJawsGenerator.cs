﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantJawsGenerator : MonoBehaviour {

    [SerializeField]
    GiantJaws GiantJawsPrefab = null;

    [SerializeField]
    float generationfrequency = 5f;

    [SerializeField]
    float probability = 30;

    float nowTimer = 0;

    [SerializeField]
    Transform RightGenerationPosition = null;

    [SerializeField]
    Transform LeftGenerationPosition = null;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        nowTimer += Time.deltaTime;
        if(nowTimer>generationfrequency)
        {
          int r=  Random.Range(0,101);//一定時間超えてどのぐらいの確率で出るか
            if(probability>r)
            {
                nowTimer = 0;
                GiantJaws g = Instantiate(GiantJawsPrefab);

                if (r%2==0)
                {
                    g.transform.position = RightGenerationPosition.position;
                    g.moveDirection = Direction.Left;
                }
                else
                {
                    g.transform.position = LeftGenerationPosition.position;
                    g.moveDirection = Direction.Right;
                }

            }
        }
	}
}
